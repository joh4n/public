import logging
import time
# logger = logging.getLogger(__name__)

import logging
from pythonjsonlogger import jsonlogger
import pythonjsonlogger

logger = logging.getLogger()
logHandler = logging.StreamHandler()
formatter = jsonlogger.JsonFormatter()
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
logger.setLevel(logging.DEBUG)

# handler = logging.StreamHandler()  # Or FileHandler or anything else
# # Configure the fields to include in the JSON output. message is the main log string itself
# format_str = '%(message)%(levelname)%(name)%(asctime)'
# formatter = pythonjsonlogger.jsonlogger.JsonFormatter(format_str)
# handler.setFormatter(formatter)
# logger = logging.getLogger('my_module_name')
# logger.addHandler(handler)
# # logger.setLevel(logging.DEBUG)
# # Normally we would attach the handler to the root logger, and this would be unnecessary
# logger.propagate = False

# i = 0
for i in range(10):
    # logger.error(f'an error {i}', extra={"tags": {"service":"my logger"}})
    # logger.info({"special": "value", "run": 12})
    # logger.info({"type": "value", "run": i})
    # logger.info({"type": "value", "epoch": i})
    # logger.info({"type": "param", "eta": i+100})
    logger.info(f"info message {i}", extra={"type": "value", "run": i})
    logger.critical(f"critical message {i}", extra={"type": "value"})
    i += 1
    time.sleep(1)
